//import { socket } from "./Main";

export class LobbyScene extends Phaser.Scene {
    private RoomIDText: Phaser.GameObjects.Text;
    private UsernameText: Phaser.GameObjects.Text;
    private RoomIDInputElement: Phaser.GameObjects.DOMElement;
    private UsernameInputElement: Phaser.GameObjects.DOMElement;
    private StartButton: Phaser.GameObjects.Text;
    private CreateRoomButton: Phaser.GameObjects.Text;

    constructor() {
        super("Lobby");
    }

    preload() {
        this.load.html("input-room-id-form", "client/Assets/HTMLAssets/InputRoomIDForm/index.html");
        this.load.html("input-username-form", "client/Assets/HTMLAssets/InputUserNameForm/index.html");
    }

    create() {
        this.add.text(100, 100, "Lobby Scene", { fontSize: "32px", color: "#fff" });

        this.RoomIDText = this.add.text(100, 200, "Room ID: ", { fontSize: "24px", color: "#fff" });
        this.UsernameText = this.add.text(100, 230, "Username: ", { fontSize: "24px", color: "#fff" });

        this.RoomIDInputElement = this.add.dom(500, 200).createFromCache("input-room-id-form");
        const RoomIDFormElement: HTMLFormElement = this.RoomIDInputElement.getChildByID("input-room-id-form") as HTMLFormElement;
        RoomIDFormElement.addEventListener("submit", (event: Event) => {
            event.preventDefault();
            const RoomIDInput = RoomIDFormElement.getElementsByTagName("input")[0] as HTMLInputElement;
            this.RoomIDText.setText(`Room ID: ${RoomIDInput.value}`);
            RoomIDInput.value = "";
        });

        this.UsernameInputElement = this.add.dom(200, 500).createFromCache("input-username-form");
        const UsernameFormElement: HTMLFormElement = this.UsernameInputElement.getChildByID("input-username-form") as HTMLFormElement;
        UsernameFormElement.addEventListener("submit", (event: Event) => {
            event.preventDefault();
            const UsernameInput = UsernameFormElement.getElementsByTagName("input")[0] as HTMLInputElement;
            this.UsernameText.setText(`Username: ${UsernameInput.value}`);
            UsernameInput.value = "";
        });

        this.StartButton = this.add.text(100, 300, "Start Game", { fontSize: "32px", color: "#fff" });
        this.StartButton.setShadow(2, 2, "#333333", 2, true, true);
        this.StartButton.setInteractive();

        this.CreateRoomButton = this.add.text(100, 350, "Create Room", { fontSize: "32px", color: "#fff" });
        this.CreateRoomButton.setShadow(2, 2, "#333333", 2, true, true);
        this.CreateRoomButton.setInteractive();
    }
}