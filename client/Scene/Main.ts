import Phaser from "phaser";
import io from "socket.io-client";

import { LobbyScene } from "./Lobby";
//import { GameScene } from "./Game";
//import { ResultScene } from "./Result";

const socket = io("localhost:3000");

const config: Phaser.Types.Core.GameConfig = {
    type: Phaser.AUTO,
    scale: {
        mode: Phaser.Scale.FIT,
        width: 1920,
        height: 1080,
    },
    backgroundColor: '#028af8',
    parent: 'app',
    physics: {
        default: 'arcade',
        arcade: {
            gravity: { y: 0 },
        },
    },
    dom: {
        createContainer: true,
    },
    scene: [LobbyScene],
};

const game = new Phaser.Game(config);
export { socket }