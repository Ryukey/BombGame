import http from 'http';
import express from 'express';
import { Server } from "socket.io";
import cors from 'cors';
import {port} from './environment/config.js';
import { handleLobbyConnection } from './LobbyHandlers.js';


const app = express();
const server = http.createServer(app);
const io = new Server(server, {
    cors: {
        origin: '*',
        methods: ['GET', 'POST']
    }
});

// Port 3000でサーバーを起動
const PORT = process.env.PORT || port;
// CORSを有効にする
app.use(cors(), express.json());

handleLobbyConnection(io);

server.listen(PORT, () => {
    console.log(`Server running on port ${PORT}`);
});
