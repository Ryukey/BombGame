import { Server } from "socket.io";

export function handleLobbyConnection(io) {
    io.on("connection", (socket) => {
        console.log("A user connected to the lobby");
        socket.on("disconnect", () => {
            console.log("A user disconnected from the lobby");
        });
    });
}
